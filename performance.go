package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

/**
 * Defines how our employee data is structured, how you like dem nested structs
 **/
type Employee [][]struct {
	Employee  int `json: "employee"`
	WorkCount int `json:"workCount"`
}

/**
 * Map of total employee work count
 **/
var totals map[int]int

/**
 * Map of frequency employee is under average
 **/
var numberOver map[int]int

var weekData map[int]int

var totalNumberOver int

/**
 * Parses all data
 **/
func parse() {
	all := Employee{}

	url := "http://bryall3/makeEmployeeData.php"

	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Couldn't retrieve stuff")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Uh oh no data!")
	}

	err = json.Unmarshal(body, &all)
	if err != nil {
		fmt.Printf("%T\n%s\n%#v\n", err, err, err)
	}

	for i := range all {
		var curTotal int
		for j := range all[i] {
			totals[all[i][j].Employee] += all[i][j].WorkCount
			curTotal += totals[all[i][j].Employee]
			//fmt.Println(all[i][j].WorkCount)
		}
		weekData[i] = curTotal
		checkAvg(curTotal, i)
	}
}

func checkAvg(C int, W int) {
	avg := C / len(totals)

	for i := range totals {
		if totals[i] < avg {
			numberOver[i] += 1
			totalNumberOver += 1
		}
	}

	// Only check if we have over 1 month of data
	if W > 4 {
		enforcer(W)
	}
}

func enforcer(W int) {
	avg := totalNumberOver / len(numberOver)
	idx := 0
	overAllFactor := 0

	for i := range numberOver {
		if numberOver[i] > avg {
			if i != idx {
				if numberOver[i] > numberOver[idx] {
					if overAllFactor == 0 && idx != 0 {
						overAllFactor = numberOver[idx]
					}
					if numberOver[idx] != 0 && (numberOver[i]-numberOver[idx]) > overAllFactor {
						fmt.Println(numberOver[i])
						fmt.Println(numberOver[idx])
						fmt.Println(numberOver[i] - numberOver[idx])
						overAllFactor = numberOver[i] - overAllFactor
					}
					idx = i
				} else if numberOver[i] == numberOver[idx] {
					idx = 0
				}
			}
		}
	}

	if idx == 0 || overAllFactor < 1 {
		return
	}

	fmt.Printf("Fire employee: %#v.\n", idx)
	fmt.Printf("Employee %#v has %#v more weeks of underperformance than peers.\n", idx, overAllFactor)
	fmt.Printf("Average # of weeks under: %#v.\n", avg)
	fmt.Println(numberOver)
	fmt.Printf("Fired on the %#vth week.\n\n", W)
	os.Exit(1)
}

func main() {
	totals = make(map[int]int)
	weekData = make(map[int]int)
	numberOver = make(map[int]int)
	parse()
}
